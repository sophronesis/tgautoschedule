# Automatic telegram scheduling

This simple script will allow you to automate routine
setup of posting schedule to a given telegram channel
from some queue channel.

# Getting started

So let's define the basics. We have one channel that we
will call "Queue" which we use to store scheduled edge
memes for them to later be posted to another channel
called "Target". The only thing this script does is it
gets a random message from "Queue" using some given
sampler function, sends it to "Target" and if it's
successfully posted - deletes an original in "Queue".
That's not rocket science, everything is plain and simple.

# Installation

0. Install `pyrogram` and `tgcrypto` packages for python.
1. Get your "api_id" and "api_hash" from https://my.telegram.org/ (Go to "API Development tools" -> "Create new application" -> "Create application" and fill the rest)
2. Put your "api_id" and "api_hash" to config.json in your folder
3. Put your username or some unique name to "username" to config.json
4. Modify the `createSchedule()` function or create your own and replace the name in `scheduleF = createSchedule` with your own function. Just remember that the scheduling
function should output a list of events (transferring of messages) for a current day.
Each event is tuple with 3 elements:
    * time of day in "hours:minutes" to post on
    * source channel name (both plain channel name and @username are valid)
    * target channel name
5. Run `src.py` and enjoy the reduction in time needed to maintain your shitposting channel

