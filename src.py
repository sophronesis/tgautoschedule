#!/usr/bin/env python3
import random
import json
import os
import asyncio
from time import sleep
import datetime
from pyrogram import Client
import logging
from collections import Counter

logging.basicConfig(level=logging.INFO, 
                    format="[%(asctime)s: %(levelname)-8s %(name)-s] %(message)s",
                    handlers=[logging.FileHandler("autoposting.log"), logging.StreamHandler()])
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

with open("config.json", "r") as f:
    cfg = json.load(f)

api_id   = cfg["api_id"]
api_hash = cfg["api_hash"]
username = cfg["username"]

# uniform sampler function
one_msg_uniform = lambda list_:[list_[random.randint(0, len(list_)-1)]]

async def isMessageForwarded(msg):
    return msg[0].forward_from_chat is not None

async def getChatIdFromName(app, chName):
    logging.debug(f"Getting chatid for '{chName}'")
    async for dialog in app.get_dialogs():
        if chName in str(dialog.chat.first_name or dialog.chat.title): 
            return dialog.chat.id
    return None

# Group album messages as one message
def groupMessages(messages):
    groups = []
    last_group = []
    last_id = 0
    for msg in messages:
        if msg.media_group_id is None:
            last_id = 0
            if len(last_group) > 0:
                groups.append(last_group)
                last_group = []
            groups.append([msg])
        elif msg.media_group_id == last_id:
            last_group.append(msg)
        elif msg.media_group_id != last_id:
            if len(last_group) > 0:
                groups.append(last_group)
            last_group = [msg]
            last_id = msg.media_group_id
    if len(last_group) > 0:
        groups.append(last_group)
    return groups

# Get sample of messages from given channel with given sampling function
async def getMessagesSample(app, chName, sampleF):
    logging.debug(f"Sampling messages from '{chName}'")
    chat_id = await getChatIdFromName(app, chName)
    messages = [msg async for msg in app.get_chat_history(chat_id) if msg.id != 1]

    # pack albums as one message
    messages = groupMessages(messages)
    if len(messages) > 0:
        return sampleF(messages)
    return None

# Get total messages count in given channel
async def getMessagesCount(app, chName):
    length = await getMessagesSample(app, chName, lambda x:len(x))
    return length if length is not None else 0    

# Send message from souce channel to target channel
async def sendMsgToChan(app, sourceID, targetID, message):
    logging.debug(f"Sending message with id={message[0].id} from '{sourceID}' to '{targetID}'")
    if await isMessageForwarded(message):
        getId = lambda x:x.id
        ids = list(map(getId, message))
        return await app.forward_messages(targetID, sourceID, ids)
    else:
        if len(message) == 1:
            return await app.copy_message(targetID, sourceID, message[0].id)
        else:
            return await app.copy_media_group(targetID, sourceID, message[0].id)

# Delete given message in source channel
async def deleteMsg(app, sourceID, message):
    logging.debug(f"Deleting message with from '{sourceID}'")
    getId = lambda x:x.id
    ids = list(map(getId, message))
    return await app.delete_messages(sourceID, ids)

# Transfering of message from queue channel to target channel
async def transferMsg(app, queue_ch, target_ch, samplerFunc):
    target_id  = await getChatIdFromName(app, target_ch)
    if target_id is None:
        logging.error(f"Target channel with name '{target_ch}' not found")
        return
    queue_id   = await getChatIdFromName(app, queue_ch)
    if queue_id is None:
        logging.error(f"Source channel with name '{queue_ch_ch}' not found")
        return
    sampleMsgs = await getMessagesSample(app, queue_ch, samplerFunc)
    logging.info(f"Transfering message from '{queue_ch}' to '{target_ch}'")
    if sampleMsgs is None:
        logging.warning(f"No messages left in source channel '{queue_ch}'")
        return
    for msg in sampleMsgs:
        try:
            res = await sendMsgToChan(app, queue_id, target_id, msg)
        except Exception as e:
            logging.warning(f"Unable to send message to '{target_ch}'. Possibly missing admin status for target channel.")
            logging.warning(e)
        else:
            # delete original message in source channel only if no errors occured 
            try:
                deleteRes = await deleteMsg(app, queue_id, msg)
                return deleteRes
            except:
                logging.warning(f"Unable to delete message from '{queue_ch}'.")

# Create schedule of posts
def createSchedule():
    now = datetime.datetime.now()
    daynow = f"{now.year}/{now.month}/{now.day}" 
    schedule = []

    # 5 posts for emergent order
    for i in range(5):
        hours = random.randint(10, 22)
        mins  = random.randint(0, 59)
        schedule.append((f"{hours:02}:{mins:02}", "stack.EO.filtered", "Виникаючий порядок ✙ #УкрТґ"))

    # Other channels can be added here to same schedule list as events
    # Format of an event: ("HH:MM", "source channel name", "target channel name")

    # sort by time
    schedule = sorted(schedule, key=lambda x:convertToTotalMins(x[0]))
    logging.info(f"Created schedule for {daynow} ({len(schedule)} events)")
    return schedule

# Log all events in given schedule
def logSchedule(schedule):
    for event in schedule:
        e_time, e_queue, e_target = event
        logging.info(f"[{e_time}] {e_queue} -> {e_target}")

# Convert time from "hours:mins" format to integer
def convertToTotalMins(x):
    return 60*int(x[:2])+int(x[-2:])

# Get current time in "hours:mins" format
def getCurrentTime():
    now = datetime.datetime.now()
    timenow = f"{now.hour:02}:{now.minute:02}"
    return timenow

# Log stats for all source channels in given schedule
async def showQueuesStats(app, schedule):
    logging.info("Queues stats: (days of consecutive posting)")
    src_chans = list(map(lambda x:x[1], schedule))
    cnt = Counter(src_chans)
    for (chan, rate) in cnt.most_common():
        count = await getMessagesCount(app, chan)
        logging.info(f"{count:5}/{rate:<2}= {count/rate:4} - {chan}")

async def main():
    # selecting one sampler to use
    samplerF  = one_msg_uniform
    scheduleF = createSchedule

    # creating basic schedule
    timenow = getCurrentTime()
    pre_schedule = scheduleF()
    cmpTime = lambda x:convertToTotalMins(x[0]) >= convertToTotalMins(timenow)
    schedule = list(filter(cmpTime, pre_schedule))
    logSchedule(schedule)

    # channels stats
    async with Client(username, api_id, api_hash) as app:
        await showQueuesStats(app, pre_schedule)

    # main loop
    while True:
        # reschedule on new day
        if timenow == "00:00" and len(schedule) == 0:
            schedule = scheduleF()
            logSchedule(schedule)

        # transfer posts if time has come
        while len(schedule) > 0 and convertToTotalMins(timenow) >= convertToTotalMins(schedule[0][0]):
            postTime, queue_ch, target_ch = schedule[0]
            schedule = schedule[1:]
            async with Client(username, api_id, api_hash) as app:
                res = await transferMsg(app, queue_ch, target_ch, samplerF)
        sleep(30)
        timenow = getCurrentTime()

asyncio.run(main())
